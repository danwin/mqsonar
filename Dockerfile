FROM phusion/baseimage:0.10.0


# Rferences:
# https://hub.docker.com/r/phusion/baseimage/tags/
# https://github.com/phusion/baseimage-docker#building
# http://phusion.github.io/baseimage-docker/

# Also:
# http://blog.xebia.com/how-to-create-the-smallest-possible-docker-container-of-any-image/
# https://github.com/mvanholsteijn/strip-docker-image

RUN set -x \
	&& apt-get update && apt-get install -y --no-install-recommends \
    git gcc git curl make\
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && mkdir tmp \
    && cd tmp \
    && git clone https://github.com/torch/luajit-rocks.git \
    && cd luajit-rocks \
    && mkdir build \
    && cd build \
    && cmake .. \
    && sudo make install

CMD ["luajit -v"]

